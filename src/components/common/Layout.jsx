import { Stack } from "@chakra-ui/react";
import React from "react";
import NavBar from "../NavBar";
import MainPageFooter from "../MainPageFooter";

const Layout = ({ children }) => {
  return (
    <>
      <Stack
        position={"sticky"}
        top={0}
        zIndex={10000}
        style={{ boxShadow: "0px 2px 4px rgba(0, 0, 0, 0.1)" }}
        as="nav"
      >
        <NavBar />
      </Stack>
      <Stack
        maxWidth={1200}
        marginInline={"auto"}
        paddingInline={{ base: "10px", xl: "0px" }}
        as="main"
      >
        {children}
      </Stack>
      <MainPageFooter />
    </>
  );
};

export default Layout;
