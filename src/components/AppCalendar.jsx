import React, { useState, useEffect } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import EventCard from "./EventCard";
import { Box, useMediaQuery } from "@chakra-ui/react";
import { useLocation } from "react-router";
import useGetEvents from "../hooks/useGetEvents";
import Loader from "./Loader";

const AppCalendar = () => {
  const { search } = useLocation(); // ?view=agenda or ?view=week
  const searchView = search.split("=")[1]; // get agenda / week from search
  const [view, setView] = useState("agenda");

  const [isMobile] = useMediaQuery("(max-width: 1000px)");

  const { events, isLoading } = useGetEvents();

  console.log(events);

  useEffect(() => {
    if (searchView) setView(searchView);
  }, [searchView]);

  const localizer = momentLocalizer(moment);
  const today = new Date();

  if (isLoading) return <Loader />;

  if (!events || events.length === 0 || !today) return null;

  return (
    <Box p={5} marginTop={"32px"}>
      <Calendar
        localizer={localizer}
        events={events}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 600 }}
        defaultView="agenda"
        view={view}
        onView={setView}
        views={isMobile ? ["agenda"] : ["week", "agenda"]}
        components={{
          event: EventCard,
        }}
      />
    </Box>
  );
};

export default AppCalendar;
