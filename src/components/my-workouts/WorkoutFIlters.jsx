import { Select } from "@chakra-ui/react";
import React from "react";

const WorkoutFIlters = ({ onFilterTypeChange }) => {
  return (
    <Select
      mb={4}
      placeholder="Filter by type"
      onChange={(e) => onFilterTypeChange(e.target.value)}
    >
      <option value="All">All</option>
      <option value="Cardio">Cardio</option>
      <option value="Strength building">Strength building</option>
      <option value="Poll dancing">Poll dancing</option>
      <option value="Stretching">Stretching</option>
      <option value="Tango">Tango</option>
      <option value="Ballet">Ballet</option>
      <option value="Ballroom dancing">Ballroom dancing</option>
      <option value="Modern">Modern</option>
    </Select>
  );
};

export default WorkoutFIlters;
