import { Flex, Table, Tbody, Td, Th, Thead, Tr } from "@chakra-ui/react";
import { FaSort, FaSortDown, FaSortUp } from "react-icons/fa";
import React from "react";
import { formatDatePretty } from "../../utils/workouts";

const WorkoutTable = ({ workouts, onSort, sortField, sortDirection }) => {
  const getSortIcon = (field) => {
    if (field === sortField) {
      return sortDirection === "asc" ? <FaSortUp /> : <FaSortDown />;
    }
    return <FaSort />;
  };

  return (
    <Table>
      <Thead>
        <Tr>
          <Th onClick={() => onSort("name")}>
            <Flex align="center" gap={4} fontSize={15}>
              Name {getSortIcon("name")}
            </Flex>
          </Th>
          <Th onClick={() => onSort("date")}>
            <Flex align="center" gap={4} fontSize={15}>
              Date {getSortIcon("date")}
            </Flex>
          </Th>
          <Th onClick={() => onSort("type")}>
            <Flex align="center" gap={4} fontSize={15}>
              Type {getSortIcon("type")}
            </Flex>
          </Th>
        </Tr>
      </Thead>
      <Tbody>
        {workouts.map((workout, index) => (
          <Tr key={index}>
            <Td>{workout.name}</Td>
            <Td>{formatDatePretty(workout.date)}</Td>
            <Td>{workout.type}</Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  );
};

export default WorkoutTable;
