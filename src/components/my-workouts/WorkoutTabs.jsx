import { Box, Input, TabPanel, TabPanels, Tabs } from "@chakra-ui/react";
import React, { useState } from "react";
import useMyWorkouts from "../../hooks/useMyWorkouts";
import Loader from "../Loader";
import Pagination from "./Pagination";
import WorkoutFilters from "./WorkoutFIlters";
import WorkoutTable from "./WorkoutTable";
import WorkoutsEmptyPlaceholder from "./WorkoutsEmptyPlaceholder";

const WorkoutTabs = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [sortField, setSortField] = useState("title");
  const [filterTypes, setFilterTypes] = useState("All");
  const [sortDirection, setSortDirection] = useState("asc");
  const [currentPage, setCurrentPage] = useState(1);
  const workoutsPerPage = 10;

  const { isLoading, myWorkouts } = useMyWorkouts();

  const filteredWorkouts = myWorkouts
    //for search
    .filter((workout) =>
      workout.title.toLowerCase().includes(searchTerm.toLowerCase())
    )
    .filter((workout) => filterTypes === "All" || workout.type === filterTypes)
    .sort((a, b) => {
      const valueA = a[sortField];
      const valueB = b[sortField];

      if (typeof valueA === "string" && typeof valueB === "string") {
        // Case-insensitive string comparison
        return (
          valueA.localeCompare(valueB, undefined, { sensitivity: "base" }) *
          (sortDirection === "asc" ? 1 : -1)
        );
      } else if (typeof valueA === "number" && typeof valueB === "number") {
        // Numeric comparison
        return (valueA - valueB) * (sortDirection === "asc" ? 1 : -1);
      } else if (valueA instanceof Date && valueB instanceof Date) {
        // Date comparison
        return (valueA - valueB) * (sortDirection === "asc" ? 1 : -1);
      } else {
        // Fallback to string comparison for other types
        return (
          String(valueA).localeCompare(String(valueB), undefined, {
            sensitivity: "base",
          }) * (sortDirection === "asc" ? 1 : -1)
        );
      }
    });

  const indexOfLastWorkout = currentPage * workoutsPerPage;
  const indexOfFirstWorkout = indexOfLastWorkout - workoutsPerPage;
  //for amount on page
  const currentWorkouts = filteredWorkouts.slice(
    indexOfFirstWorkout,
    indexOfLastWorkout
  );

  const totalPages = Math.ceil(filteredWorkouts.length / workoutsPerPage);

  const handleSort = (field) => {
    setSortDirection((prevSortDirection) => {
      // If clicking on the same field, toggle the sort direction
      if (field === sortField) {
        return prevSortDirection === "asc" ? "desc" : "asc";
      } else {
        // If clicking on a different field, set it as the new sort field and default to ascending order
        setSortField(field);
        return "asc";
      }
    });
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  if (isLoading) return <Loader />;

  return (
    <Box p={4} marginTop={10} pb={"100px"}>
      <Input
        mb={4}
        placeholder="Search by title"
        onChange={(e) => setSearchTerm(e.target.value)}
      />
      <WorkoutFilters onFilterTypeChange={setFilterTypes} />
      <Tabs>
        <TabPanels>
          <TabPanel>
            {currentWorkouts.length === 0 ? (
              <WorkoutsEmptyPlaceholder />
            ) : (
              <>
                <WorkoutTable
                  workouts={currentWorkouts}
                  onSort={handleSort}
                  sortDirection={sortDirection}
                  sortField={sortField}
                />
                <Pagination
                  currentPage={currentPage}
                  totalPages={totalPages}
                  onPageChange={handlePageChange}
                />
              </>
            )}
          </TabPanel>
        </TabPanels>
      </Tabs>
    </Box>
  );
};

export default WorkoutTabs;
