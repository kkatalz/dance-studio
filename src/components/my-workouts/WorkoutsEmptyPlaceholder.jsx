import { Text } from "@chakra-ui/react";
import React from "react";

const WorkoutsEmptyPlaceholder = () => {
  return (
    <Text
      fontSize="lg"
      fontWeight="bold"
      color="gray.500"
      textAlign="center"
      mt={8}
    >
      Oops! No workouts available.
    </Text>
  );
};

export default WorkoutsEmptyPlaceholder;
