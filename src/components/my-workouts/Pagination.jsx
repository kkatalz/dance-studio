import { Button, HStack } from "@chakra-ui/react";
import React from "react";

const Pagination = ({ currentPage, totalPages, onPageChange }) => {
  const pages = Array.from({ length: totalPages }, (_, i) => i + 1);

  if (pages.length < 2) return null;

  return (
    <HStack m={4} spacing={2}>
      {pages.map((page) => (
        <Button
          key={page}
          onClick={() => onPageChange(page)}
          bgColor={currentPage === page ? "#F3C0A1" : "lightgray"}
          color={currentPage === page ? "white" : "black"}
        >
          {page}
        </Button>
      ))}
    </HStack>
  );
};

export default Pagination;
