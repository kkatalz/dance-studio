import { Stack } from "@chakra-ui/react";
import React from "react";
import ReactPlayer from "react-player/youtube";
const danceVideoURL = "https://www.youtube.com/watch?v=7zrqjb77QaE";

const MainVideoPlayer = () => {
  return (
    <Stack marginTop={"30px"}>
      <ReactPlayer
        url={danceVideoURL}
        playing
        muted
        width="100%"
        height={580}
        marginInline={"auto"}
      />
    </Stack>
  );
};

export default MainVideoPlayer;
