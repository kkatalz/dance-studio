import React, { useState } from "react";
import { Box, Text, Flex, Badge } from "@chakra-ui/react";
import { addWorkout } from "../services/my-workouts";

import { ToastContainer, toast } from "react-toastify";
const EventCard = ({ event: eventData }) => {
  const [event, setEvent] = useState(eventData);

  if (!event || !event.title) {
    // Handle the case where the event or title is undefined
    return <div>Error: Event title not available</div>;
  }
  const handleSuccess = () => toast.success("Successfully took class!");
  const handleError = () => toast.error("This class is already taken!");

  const handleClick = async () => {
    const res = await addWorkout(event, event.id);
    if (res.ok && event.emptyPlaces > 0) {
      handleSuccess();
      setEvent({ ...event, emptyPlaces: event.emptyPlaces - 1, checked: true });
    } else {
      handleError();
    }
  };

  const isDisabled = event.emptyPlaces < 1;

  return (
    <Box
      p={4}
      borderWidth="1px"
      borderRadius="lg"
      bg={event.checked ? "lightgray" : "white"}
      boxShadow="md"
      transition="transform 0.2s"
      _hover={{ transform: "scale(1.02)" }}
      onClick={handleClick}
      disabled={isDisabled}
    >
      <Text fontSize="l" fontWeight="bold" mb={2} color={"black"}>
        {event.title}
      </Text>
      {/* <Text color={"black"}>{event.type}</Text> */}
      <Flex justify="space-between" mt={2}>
        <Badge colorScheme="green">
          Places: {event.emptyPlaces}/{event.places}
        </Badge>
      </Flex>
    </Box>
  );
};

export default EventCard;
