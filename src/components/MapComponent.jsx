import { Box } from "@chakra-ui/react";
import L from "leaflet";
import "leaflet/dist/leaflet.css";
import React from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { getLocations } from "../data/locationsInfo";
import useGeoLocation from "../hooks/useGeoLocation";
import Pin from "../images/pin.svg";

const customIcon = new L.Icon({
  iconUrl: Pin,
  iconSize: [32, 32],
  iconAnchor: [16, 32],
  popupAnchor: [0, -32],
});

const MapComponent = () => {
  const { position } = useGeoLocation();

  const locations = getLocations(position);

  if (position[0] === 0) return null;

  return (
    <Box style={{ borderRadius: "20px", overflow: "hidden" }}>
      <MapContainer
        center={position}
        zoom={12}
        style={{ height: "400px", width: "100%" }}
      >
        <TileLayer
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        />
        {Object.keys(locations).map((key) => (
          <Marker
            key={key}
            position={locations[key].position}
            icon={customIcon}
          >
            <Popup>{locations[key].address}</Popup>
          </Marker>
        ))}
      </MapContainer>
    </Box>
  );
};

export default MapComponent;
