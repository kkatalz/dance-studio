import React, { useState } from "react";
import {
  Box,
  Heading,
  Stack,
  Text,
  Button,
  Select,
  Divider,
  Drawer,
  DrawerBody,
  DrawerOverlay,
  DrawerContent,
  DrawerCloseButton,
  useDisclosure,
} from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { useNavigate } from "react-router";

const NavBar = () => {
  const [selectedOption, setSelectedOption] = useState("");
  const { isOpen, onOpen, onClose } = useDisclosure();
  const navigate = useNavigate();

  const handleChange = (event) => {
    const selectedValue = event.target.value;
    setSelectedOption(selectedValue);

    // Perform navigation based on the selected option
    if (selectedValue === "week") {
      navigate("/schedule?view=week");
    } else if (selectedValue === "agenda") {
      navigate("/schedule?view=agenda");
    }
  };

  const MobileMenu = () => (
    <Drawer placement="right" onClose={onClose} isOpen={isOpen}>
      <DrawerOverlay zIndex={11111}>
        <DrawerContent>
          <DrawerCloseButton />
          <DrawerBody paddingTop={10}>
            <Stack spacing={4}>
              <Select
                variant="unstyled"
                cursor={"pointer"}
                fontSize={18}
                fontWeight={"bold"}
                placeholder="SCHEDULE"
                maxWidth={"200px"}
                value={selectedOption}
                onChange={handleChange}
              >
                <option value="agenda">Agenda</option>
              </Select>
              <Link to={"/my-workouts"}>
                <Text fontSize={18} fontWeight={"bold"} cursor={"pointer"}>
                  MY WORKOUTS
                </Text>
              </Link>
              <Link to={"/contacts"}>
                <Text fontSize={18} fontWeight={"bold"} cursor={"pointer"}>
                  CONTACTS
                </Text>
              </Link>
            </Stack>
          </DrawerBody>
        </DrawerContent>
      </DrawerOverlay>
    </Drawer>
  );

  return (
    <Stack
      flexDirection={"row"}
      alignItems={"center"}
      padding={8}
      background={"white"}
      justifyContent={"space-between"}
    >
      <Link to={"/"}>
        <Heading cursor={"pointer"}>DANCE STUDIO</Heading>
      </Link>
      <Stack
        flexDirection={"row"}
        fontSize={18}
        fontWeight={"bold"}
        gap={8}
        whiteSpace={"nowrap"}
        alignItems={"center"}
        display={{ base: "none", xl: "flex" }} // Hide on mobile, show on desktop
      >
        <Select
          variant="unstyled"
          cursor={"pointer"}
          fontSize={18}
          fontWeight={"bold"}
          placeholder="SCHEDULE"
          maxWidth={"200px"}
          value={selectedOption}
          onChange={handleChange}
        >
          <option value="week">Week</option>
          <option value="agenda">Agenda</option>
        </Select>

        <Divider
          orientation="vertical"
          borderColor="black"
          height={8}
          padding={0}
        />
        <Link to={"/my-workouts"}>
          <Text fontSize={18} fontWeight={"bold"} cursor={"pointer"}>
            MY WORKOUTS
          </Text>
        </Link>

        <Divider orientation="vertical" borderColor="black" height={8} />
        <Link to={"/contacts"}>
          <Text fontSize={18} fontWeight={"bold"} cursor={"pointer"}>
            CONTACTS
          </Text>
        </Link>
      </Stack>
      <Button
        fontSize={18}
        fontWeight={"bold"}
        padding={7}
        backgroundColor={"#F3C0A1"}
        onClick={() => navigate("/schedule")}
        display={{ base: "none", xl: "flex" }} // Hide on mobile, show on desktop
      >
        TAKE CLASS
      </Button>
      {/* Mobile Menu Icon */}
      <Button
        alignItems={"center"}
        justifyContent={"center"}
        fontSize={18}
        fontWeight={"bold"}
        padding={7}
        backgroundColor={"#F3C0A1"}
        onClick={onOpen}
        display={{ base: "flex", xl: "none" }} // Show on mobile, hide on desktop
      >
        ☰
      </Button>
      {/* Mobile Menu */}
      <MobileMenu />
    </Stack>
  );
};

export default NavBar;
