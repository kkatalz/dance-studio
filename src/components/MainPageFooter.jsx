import { Button, Icon, Link, Stack, Text } from "@chakra-ui/react";
import React from "react";
import { FaFacebookSquare, FaInstagram } from "react-icons/fa";
import { useNavigate, useLocation } from "react-router";

const MainPageFooter = () => {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const isShowContactsButton = pathname !== "/contacts";

  return (
    <Stack
      backgroundColor="#333"
      color="white"
      padding={8}
      flexDirection={{ base: "column", md: "row" }}
      justifyContent={"space-between"}
      marginTop={"200px"}
      as="footer"
    >
      <Stack spacing={4}>
        <Text fontSize={25} fontWeight={"bold"}>
          Find us here
        </Text>
        <Stack flexDirection={"row"}>
          <Icon as={FaInstagram} boxSize={6} />
          <Text>https://www.instagram.com/dancestudio</Text>
        </Stack>

        <Stack flexDirection={"row"}>
          <Icon as={FaFacebookSquare} boxSize={6} />
          <Text>https://www.facebook.com/dancestudio</Text>
        </Stack>
      </Stack>

      {isShowContactsButton ? (
        <Button
          fontSize={18}
          fontWeight={"bold"}
          padding={7}
          backgroundColor={"#F3C0A1"}
          alignItems={"center"}
          onClick={() => navigate("/contacts")}
          flexDirection={{}}
          marginTop={{ base: "30px", md: "15px" }}
        >
          Contacts
        </Button>
      ) : null}
    </Stack>
  );
};

export default MainPageFooter;
