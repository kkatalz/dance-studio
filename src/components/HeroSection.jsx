import { Heading, Image, Stack } from "@chakra-ui/react";
import React from "react";
import ImageFirst from "../images/dance1.jpg";
import ImageSecond from "../images/dance2.jpg";
import ImageThird from "../images/dance3.jpg";
import ImageFourth from "../images/dance4.jpg";

const HeroSection = () => {
  return (
    <Stack
      marginTop={20}
      flexDirection={{ base: "column", md: "row" }}
      justifyContent={"space-between"}
      alignItems={{ base: "center", md: "flex-start" }}
      gap={{ base: "40px", md: "0px" }}
      maxWidth={{ base: "100%", md: "80%" }}
      marginInline={"auto"}
    >
      <Stack width={"50%"} textAlign={{ base: "center", md: "center" }}>
        <Heading>About us</Heading>
        <Heading marginTop={5} size={14} fontWeight={"normal"}>
          Welcome to our vibrant dance studio, where movement meets artistry!
          Immerse yourself in a dynamic space designed for creativity and
          self-expression. Our state-of-the-art facility blends modern
          aesthetics with a welcoming atmosphere, fostering a community
          passionate about dance. From beginner to advanced levels, our expert
          instructors guide you through various dance styles, igniting your
          passion for rhythm and movement.
        </Heading>
      </Stack>

      <Stack height={"300px"} width={"300px"}>
        <Stack flexDirection={"row"}>
          <Image
            src={ImageFirst}
            width={"156px"}
            height={"120px"}
            objectFit={"cover"}
            borderRadius={10}
          />
          <Image
            src={ImageSecond}
            width={"136px"}
            height={"120px"}
            objectFit={"cover"}
            borderRadius={10}
          />
        </Stack>
        <Stack flexDirection={"row"}>
          <Image
            src={ImageThird}
            width={"140px"}
            height={"172px"}
            objectFit={"cover"}
            borderRadius={10}
          />
          <Image
            src={ImageFourth}
            width={"160px"}
            height={"172px"}
            objectFit={"cover"}
            borderRadius={10}
          />
        </Stack>
      </Stack>
    </Stack>
  );
};

export default HeroSection;
