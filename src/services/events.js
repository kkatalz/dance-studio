import { baseURL } from "./baseUrl";

const fetchEvents = async () => {
  try {
    const response = await fetch(`${baseURL}/events`);
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }

    const events = await response.json();
    return events;
  } catch (error) {
    console.error("Error fetching events:", error.message);
    throw error;
  }
};

export { fetchEvents };
