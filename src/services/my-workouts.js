import { baseURL } from "./baseUrl";

const checkWorkoutAvailability = async (event, chosenEventId) => {
  try {
    // Fetch my workouts
    const myWorkouts = await fetchMyWorkouts();

    // Check if empty places are less than 1
    if (event.emptyPlaces < 1) {
      console.log("Cannot add workout. No empty places available.");
      return false;
    }

    // Check if the workout already exists in my-workouts
    const existingWorkout = myWorkouts.find(
      (workout) => workout.id === chosenEventId
    );

    if (existingWorkout) {
      console.log("Workout already exists in my-workouts.");
      return false;
    }

    return true; // Workout can be added
  } catch (error) {
    console.error("Error checking workout availability:", error.message);
    throw error;
  }
};

const addWorkout = async (event, chosenEventId) => {
  try {
    // Check if the workout can be added
    const canAddWorkout = await checkWorkoutAvailability(event, chosenEventId);

    if (!canAddWorkout) {
      console.log("Workout cannot be added.");
      return { ok: false };
    } else {
      // POST request to create a new my-workout
      const createMyWorkoutResponse = await fetch(`${baseURL}/my-workouts`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(event),
      });

      await fetch(`${baseURL}/events/${chosenEventId}`, {
        method: "PUT", // Assuming your server uses PUT for updating
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          ...event,
          emptyPlaces: event.emptyPlaces - 1,
          checked: true,
        }), // Reduce emptyPlaces by 1
      });

      return createMyWorkoutResponse;
    }
  } catch (error) {
    console.error("Error:", error.message);
    throw error;
  }
};

const fetchMyWorkouts = async () => {
  try {
    const response = await fetch(`${baseURL}/my-workouts`);

    const events = await response.json();
    return events;
  } catch (error) {
    console.error("Error fetching workouts:", error.message);
    throw error;
  }
};

export { addWorkout, fetchMyWorkouts };
