export const getLocations = (position) => {
  return {
    current: {
      position: position,
      address: "My current location.",
    },
    kyiv: {
      position: [50.40913, 30.6266],
      address: "проспект Петра Григоренка, 22/20, Київ, 02081",
    },
    lviv: {
      position: [49.8397, 24.0297],
      address: "Lviv, Ukraine",
    },
    lviv1: {
      position: [49.83723, 24.03586],
      address:
        "Вулиця Академіка Богомольця, 6, Львів, Львівська Область, 79000",
    },
    kyiv1: {
      position: [50.46289, 30.517],
      address: "Kontractova Street,8, Kyiv, Ukraine",
    },
  };
};
