export const workoutsData = [
  { name: "Workout B", date: "2023-01-02", type: "Stretching" },
  { name: "Workout A", date: "2023-01-01", type: "Ballet" },
  { name: "Workout B", date: "2023-01-02", type: "Stretching" },
  { name: "Workout B", date: "2023-01-02", type: "Stretching" },
  { name: "Workout B", date: "2023-01-02", type: "Stretching" },
  // Add more workout data as needed
];
