import React, { useState } from "react";
import Layout from "../components/common/Layout";
import { Box, Heading, Text, Flex, Grid, GridItem } from "@chakra-ui/react";
import { MdCall } from "react-icons/md";
import MapComponent from "../components/MapComponent";

const locations = [
  {
    name: "Kyiv Studio Podil",
    address: "123 Kontractova Street, Kyiv, Ukraine",
    phone: "(123) 456-7890",
    email: "kyivKontractova@dancestudio.com",
  },

  {
    name: "Kyiv Studio Darnytsia ",
    address: "проспект Петра Григоренка, 22/20, Київ, 02081",
    phone: "(123) 456-7891",
    email: "kyivDarnytsia@dancestudio.com",
  },
  {
    name: "Lviv Studio",
    address: "456 Dance Avenue, Lviv, Ukraine",
    phone: "(234) 567-8901",
    email: "lviv@dancestudio.com",
  },

  {
    name: "Lviv Studio Center",
    address: "вулиця Академіка Богомольця, 6, Львів, Львівська область, 79000",
    phone: "(234) 567-8902",
    email: "lvivCenter@dancestudio.com",
  },
];

const ContactsPage = () => {
  const getRandomPhoneNumber = () => {
    // Get a random location from the list
    const randomLocation =
      locations[Math.floor(Math.random() * locations.length)];

    // Extract and clean the phone number
    const phoneNumber = randomLocation.phone.replace(/\D/g, "");

    // Navigate to the phone call
    window.location.href = `tel:${phoneNumber}`;
  };

  return (
    <Layout>
      <Box p={8}>
        <Heading as="h1" size="xl" mb={4}>
          Contact Us
        </Heading>

        <Text mb={4}>
          Join us in any club you want. Looking forward to seeing you soon!
        </Text>

        <Grid templateColumns="repeat(1, 1fr)" gap={6}>
          {locations.map((location, index) => (
            <GridItem key={index}>
              <Box
                p={4}
                borderWidth="1px"
                borderRadius="lg"
                boxShadow="lg"
                transition="transform 0.2s"
                _hover={{ transform: "scale(1.05)" }}
              >
                <Heading as="h2" size="lg" mb={2}>
                  {location.name}
                </Heading>
                <Text fontSize="md" mb={2}>
                  {location.address}
                </Text>
                <Text fontSize="md" mb={2}>
                  Phone: {location.phone}
                </Text>
                <Text fontSize="md">Email: {location.email}</Text>
              </Box>
            </GridItem>
          ))}
        </Grid>
      </Box>

      <Box
        cursor={"pointer"}
        transition="all 0.3s"
        _hover={{ backgroundColor: "#FFE4E1" }}
        borderRadius={"50%"}
        width={"50px"}
        height={"50px"}
        backgroundColor={"#F6B7C2"}
        display={"flex"}
        justifyContent={"center"}
        alignItems={"center"}
        position={"fixed"}
        right={"30px"}
        bottom={"30px"}
        onClick={getRandomPhoneNumber}
        zIndex={11111}
      >
        <MdCall size={"25px"} color="white" />
      </Box>

      <MapComponent></MapComponent>
    </Layout>
  );
};

export default ContactsPage;
