import React from "react";
import Layout from "../components/common/Layout";
import AppCalendar from "../components/AppCalendar";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
const SchedulePage = () => {
  return (
    <Layout>
      <AppCalendar />
      <ToastContainer />
    </Layout>
  );
};

export default SchedulePage;
