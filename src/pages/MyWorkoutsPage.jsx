import React from "react";
import Layout from "../components/common/Layout";
import WorkoutTabs from "../components/my-workouts/WorkoutTabs";

const MyWorkoutsPage = () => {
  return (
    <Layout>
      <WorkoutTabs />
    </Layout>
  );
};

export default MyWorkoutsPage;
