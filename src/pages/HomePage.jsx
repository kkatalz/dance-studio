import React from "react";
import MainVideoPlayer from "../components/MainVideoPlayer";
import Layout from "../components/common/Layout";
import HeroSection from "../components/HeroSection";
import MainPageFooter from "../components/MainPageFooter";
import { Stack } from "@chakra-ui/react";

const HomePage = () => {
  return (
    <>
      <Layout>
        <MainVideoPlayer />
        <HeroSection />
      </Layout>
    </>
  );
};

export default HomePage;
