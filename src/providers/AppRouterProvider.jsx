import React from "react";
import HomePage from "../pages/HomePage";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import MyWorkoutsPage from "../pages/MyWorkoutsPage";
import ContactsPage from "../pages/ContactsPage";
import SchedulePage from "../pages/SchedulePage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <HomePage />,
  },

  { path: "/schedule", element: <SchedulePage /> },

  {
    path: "/my-workouts",
    element: <MyWorkoutsPage />,
  },

  {
    path: "/contacts",
    element: <ContactsPage />,
  },
]);

const AppRouterProvider = () => {
  return <RouterProvider router={router} />;
};

export default AppRouterProvider;
