import { useState, useEffect } from "react";
import { fetchEvents } from "../services/events";

const useGetEvents = () => {
  let [events, setEvents] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  const fetchEventsData = async () => {
    try {
      setIsLoading(true);
      const fetchedEvents = await fetchEvents();
      setEvents(fetchedEvents);
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      // Handle error if needed
      console.error("Error fetching events:", error.message);
    }
  };

  useEffect(() => {
    fetchEventsData();
  }, []);

  const refetch = () => {
    fetchEventsData();
  };

  events = events.map((event) => ({
    ...event,
    start: new Date(event.start),
    end: new Date(event.end),
  }));

  return { events, isLoading, refetch };
};

export default useGetEvents;
