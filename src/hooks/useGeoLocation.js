import { useEffect, useState } from "react";

const useGeoLocation = () => {
  const [position, setPosition] = useState([0, 0]);

  useEffect(() => {
    // Use the Geolocation API to get the current location
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (pos) => {
          const { latitude, longitude } = pos.coords;
          setPosition([latitude, longitude]);
        },
        (err) => {
          console.error(err.message);
        }
      );
    } else {
      console.error("Geolocation is not supported by this browser.");
    }
  }, []);

  return { position };
};
export default useGeoLocation;
