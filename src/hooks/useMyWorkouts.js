import { useEffect, useState } from "react";
import { fetchMyWorkouts } from "../services/my-workouts";

const useMyWorkouts = () => {
  let [myWorkouts, setMyWorkouts] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  const fetchMyWorkoutsData = async () => {
    try {
      setIsLoading(true);
      const fetchedmyWorkouts = await fetchMyWorkouts();
      setMyWorkouts(fetchedmyWorkouts); //return array of workouts
      setIsLoading(false);
    } catch (error) {
      setIsLoading(false);
      // Handle error if needed
      console.error("Error fetching myWorkouts:", error.message);
    }
  };

  useEffect(() => {
    fetchMyWorkoutsData();
  }, []);

  myWorkouts = myWorkouts.map((event) => ({
    ...event,
    date: event.start,
    name: event.title,
  }));

  return { myWorkouts, isLoading };
};

export default useMyWorkouts;
