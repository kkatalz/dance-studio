import { formatDistanceToNow, format } from "date-fns";
import { enUS } from "date-fns/locale";

export const formatDatePretty = (dateString) => {
  const date = new Date(dateString);
  const distance = formatDistanceToNow(date, {
    addSuffix: true,
    locale: enUS,
  });

  // If the distance is less than a day, display it in distance format
  if (distance.includes("day")) {
    return distance;
  }

  // Otherwise, format the date in a pretty format
  return format(date, "dd MMMM yyyy", { locale: enUS });
};
